%define nextanimal 0

%macro  colon 2 
	%ifid %2
		%ifstr %1  
        		%2: 
			dq nextanimal
			db %1, 0
			%define nextanimal %2
 		%else
			%error  "First argument must be string!" 
        	%endif 
	%else
		%error  "Second argument must be link!" 
        %endif 
%endmacro
