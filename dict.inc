find_wrd:
   mov r10, rdi
loopfind:
    
    add rdi, 8
    push r10
    
    call string_equals
    pop r10
    test rax, rax
    je cont 
    mov rax, r10
    ret
    
cont:
    mov r10, [r10] 
    test r10, r10
    je endfind
    mov rdi, r10
    jmp loopfind
endfind:
    xor rax, rax
    ret
