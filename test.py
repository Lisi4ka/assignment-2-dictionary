import subprocess
inputs = ["cat", "frog", "capybara", "rrr", "", "dlinny input", "ewifudvwurbfufsdbvhjruebsvdhriwkdnvsbrueodlfkvncbreehldfkvcmnbrjklsefvn,ckbroiew;fld.df,kjeghrwehdfrjdfhbvurvkjdbhfeyjwhfdhjfwdfjhvebrjfjdshfbverjwhdgvbhejrwgdfbhjregdbhvregdyvhergsdfhjegrvjshcgerhjsfdhjsfbewjhdnfgsjhegfdhjwgfehdhjfgbdfcwiuehbfjweebjshfuerrr"]
outputs = ["animal2", "animal4", "animal3", "", "", "", ""]
erroutputs = ["", "", "", "There is no such word in dictionary!", "There is no such word in dictionary!", "There is no such word in dictionary!", "Input word is too long!"]
fail = False
for i in range (len(inputs)):
    p = subprocess.Popen(["./main"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, errout = p.communicate(input=inputs[i].encode())
    out = out.decode().strip()
    errout = errout.decode().strip()
    if out == outputs[i] and errout == erroutputs[i]:
        print("Test " + str(i+1) + " passed! ")
    else:
        fail = True
        if outputs[i] != "":
            print("Test " + str(i+1) + " failed! Expected output \"" + outputs[i] + "\" got \"" + out + "\"")
        else:
            print("Test " + str(i+1) + " failed! Expected stderr \"" + erroutputs[i] + "\" got \"" + errout + "\"")
if fail:
    print("Not all tests passed! Try again!")
else:
    print("Good job! All tests passed!")
