build_all: 
	nasm -f elf64 -g -F dwarf lib.inc -o lib.o
	nasm -f elf64 -g -F dwarf main.asm -o main.o
	ld -o main main.o lib.o

run:
	./main

test:
	python3 test.py