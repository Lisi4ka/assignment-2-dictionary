extern string_equals, read_word, string_length, print_string
%include "words.inc"
%include "dict.inc"
global _start

section .data
%define errmsg `There is no such word in dictionary!`
errmessage: db errmsg
%strlen errlenght errmsg
%define errmsglong `Input word is too long!`
errmessagelong: db errmsglong
%strlen errlenghtlong errmsglong


section .bss
word_buf: resb 256

section .text
_start:
    mov rdi, word_buf
    mov rsi, 255 
    call read_word
    test rax, rax
    je too_long_input
    mov rdi, nextanimal
    mov rsi, word_buf
    push rdx
    call find_wrd
    pop rdx
    test rax, rax
    je errdictend
    add rax, 8
    mov rdi, rax
    add rdi, rdx
    inc rdi
    call print_string
    jmp enddd
enddd:
    mov rax, 60
    xor rdi, rdi
    syscall
errdictend:
    mov rdx, errlenght
    mov rsi, errmessage
    call print_error
    jmp enddd
too_long_input:
    mov rdx, errlenghtlong
    mov rsi, errmessagelong
    call print_error
    jmp enddd
print_error:
    mov rdi, 2
    mov rax, 1
    syscall
    ret


